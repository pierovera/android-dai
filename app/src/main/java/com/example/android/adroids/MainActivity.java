
package com.example.android.adroids;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText usuario, contraseña;


    @Override
    protected void onCreate(Bundle savedInstanceState) { //Se recuperan los valores de los campos de texto
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usuario=findViewById(R.id.etUsuario);
        contraseña=findViewById(R.id.etContraseña);
    }

    public void irCrear(View v){ // Va a la pantalla de crear cuenta
        Intent i = new Intent(this, Crea.class);
        startActivity(i);
    }

    public void iniciaSesion(View v){ // Se intenta iniciar sesión y si no es posible, se marca el error adecuado
        Conexion con = new Conexion(this, "ProyectoDiabetes", null, 1);
        SQLiteDatabase db = con.getWritableDatabase();
        Cursor cur = db.rawQuery("select contraseña from usuario where idUsuario= '"+usuario.getText().toString()+"'",null);
        if (cur.moveToNext()){
            String contraseñaPosible=cur.getString(0);

            if(contraseña.getText().toString().equals(contraseñaPosible)){
                Toast.makeText(this, "Contraseña Correcta", Toast.LENGTH_LONG).show();
                Intent i = new Intent(this, Menu.class);
                Bundle bundle = new Bundle();
                bundle.putString("idUsuario", usuario.getText().toString());
                i.putExtras(bundle);
                startActivity(i);
            }
            else{
                Toast.makeText(this, "Contraseña INCORRECTA", Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(this, "Usuario NO registrado", Toast.LENGTH_LONG).show();
        }

        cur.close();
    }
}
