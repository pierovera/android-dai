package com.example.android.adroids;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class VerNiveles extends AppCompatActivity {

    TextView glucosa, azucar, carb, grasas, usuario,fecha;
    Spinner registros;
    Bundle datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) { //Se recuperan los valores de los campos de texto
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_niveles);
        glucosa=findViewById(R.id.tvNivelGlucosa);
        azucar=findViewById(R.id.tvAzucar);
        carb=findViewById(R.id.tvCarb);
        grasas=findViewById(R.id.tvGrasas);
        usuario=findViewById(R.id.txUsuario);
        fecha=findViewById(R.id.tvFecha);
        datos = this.getIntent().getExtras();
        String este = datos.getString("idUsuario");
        usuario.setText(este);

        llenarDrop();

    }

    public void llenarDrop(){ // Llena el drop down list con los registros de la base de datos
        registros = (Spinner) findViewById(R.id.ddRegistros);
        List<String> list = new ArrayList<String>();

        Conexion con = new Conexion(this, "ProyectoDiabetes", null, 1);
        SQLiteDatabase db = con.getWritableDatabase();
        Cursor cur = db.rawQuery("select idGlucosa from glucosa where idUsuario='"+usuario.getText().toString()+"'",null);

        while(cur.moveToNext()){
            list.add(cur.getString(0));
        }
        cur.close();

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        registros.setAdapter(dataAdapter);

        registros.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String datoSelec = (String) parent.getItemAtPosition(position);
                String[] resp=infoNiveles(datoSelec);

                glucosa.setText(resp[0]);
                azucar.setText(resp[1]);
                carb.setText(resp[2]);
                grasas.setText(resp[3]);
                fecha.setText(resp[4]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public String[] infoNiveles(String registro) { // Despliega la información de los niveles con el id seleccionado
        String[] resp=new String[5];
        Conexion con = new Conexion(this, "ProyectoDiabetes", null, 1);
        SQLiteDatabase db = con.getWritableDatabase();
        Cursor cur = db.rawQuery("select nivelG,recAzucar,recCarb,recGrasas,fecha from glucosa where idGlucosa= "+registro+" and idUsuario='"+usuario.getText().toString()+"'",null);
        if(cur.moveToNext()){
            resp[0]=cur.getString(0);
            resp[1]=cur.getString(1);
            resp[2]=cur.getString(2);
            resp[3]=cur.getString(3);
            resp[4]=cur.getString(4);
        }
        else{
            Toast.makeText(this, "No existe tal registro. Intentar de nuevo", Toast.LENGTH_LONG).show();
        }

        cur.close();

        return resp;
    }

    public void irMenu(View v){ // Regresa al menú
        Intent i = new Intent(this, Menu.class);
        i.putExtras(datos);
        startActivity(i);
    }
}
