package com.example.android.adroids;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Niveles extends AppCompatActivity {

    EditText glucosa, azucar, grasa, carb,fecha;
    TextView usuario;
    Bundle datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) { //Se recuperan los valores de los campos de texto
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_niveles);
        glucosa=findViewById(R.id.etGlucosa);
        azucar=findViewById(R.id.etAzucar);
        carb=findViewById(R.id.etCarb);
        fecha=findViewById(R.id.etFecha);
        grasa=findViewById(R.id.etGrasas);
        usuario=findViewById(R.id.txUsuario);
        datos = this.getIntent().getExtras();
        String este = datos.getString("idUsuario");
        usuario.setText(este);
    }

    public void altaNiveles(View v){ // Se registran los niveles y si no es posible, se marca error
        Conexion con2 = new Conexion(this, "ProyectoDiabetes", null, 1);
        SQLiteDatabase db2 = con2.getWritableDatabase();
        ContentValues registro = new ContentValues();
        registro.put("idGlucosa", Integer.toString(genId(con2)));
        registro.put("nivelG", glucosa.getText().toString());
        registro.put("recAzucar", azucar.getText().toString());
        registro.put("recCarb", carb.getText().toString());
        registro.put("recGrasas", grasa.getText().toString());
        registro.put("fecha",fecha.getText().toString());
        registro.put("idUsuario",usuario.getText().toString());
        if(db2.insert("glucosa", null, registro)>0)
            Toast.makeText(this, "Se agregó el registro", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, "NO se agregó.", Toast.LENGTH_LONG).show();
    }

    public void irMenu(View v){ // Regresa al menú
        Intent i = new Intent(this, Menu.class);
        i.putExtras(datos);
        startActivity(i);
    }

    private int genId(Conexion con) { // Se obtiene el id del registro de niveles
        int id = 100;
        SQLiteDatabase db = con.getWritableDatabase();
        Cursor cur = db.rawQuery("select idGlucosa from glucosa order by idGlucosa desc", null);
        if (cur.moveToNext()) {
            id = cur.getInt(0) + 1;
        }
        cur.close();

        return id;
    }
}
