package com.example.android.adroids;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NuevasRecetas extends AppCompatActivity {

    EditText nombre, ingredientes, instrucciones, carbs;
    TextView usuario;
    Bundle datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) { //Se recuperan los valores de los campos de texto
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevas_recetas);
        nombre=findViewById(R.id.etNombreRN);
        ingredientes=findViewById(R.id.etIngredientes);
        instrucciones=findViewById(R.id.etInstrucciones);
        carbs=findViewById(R.id.etCarbR);
        usuario=findViewById(R.id.txUsuario);
        datos = this.getIntent().getExtras();
        String este = datos.getString("idUsuario");
        usuario.setText(este);
    }

    public void agregaReceta(View v){ // Se registra la receta y si no es posible, se marca error
        Conexion con = new Conexion(this, "ProyectoDiabetes", null, 1);
        SQLiteDatabase db = con.getWritableDatabase();
        ContentValues registro = new ContentValues();
        registro.put("idReceta", Integer.toString(genId(con)));
        registro.put("nombreR", nombre.getText().toString());
        registro.put("ingredientes", ingredientes.getText().toString());
        registro.put("instrucciones", instrucciones.getText().toString());
        registro.put("instrucciones", instrucciones.getText().toString());
        registro.put("carbohidratos", carbs.getText().toString());
        registro.put("idUsuario",usuario.getText().toString());
        if (db.insert("receta", null, registro)>0)
            Toast.makeText(this, "Se agregó la receta", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, "NO se agregó la receta", Toast.LENGTH_LONG).show();
    }

    public void irMenu(View v){ // Regresa al menú
        Intent i = new Intent(this, Menu.class);
        i.putExtras(datos);
        startActivity(i);
    }

    private int genId(Conexion con) { // Se genera el id de la receta
        int id = 100;
        SQLiteDatabase db = con.getWritableDatabase();
        Cursor cur = db.rawQuery("select idReceta from receta order by idReceta desc", null);
        if (cur.moveToNext()) {
            id = cur.getInt(0) + 1;
        }
        cur.close();

        return id;
    }
}
