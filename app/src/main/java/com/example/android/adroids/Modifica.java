package com.example.android.adroids;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Modifica extends AppCompatActivity {

    EditText correo;
    TextView usuario;
    Bundle datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) { //Se recuperan los valores de los campos de texto
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifica);
        correo=findViewById(R.id.etNuevoCorreo);
        usuario=findViewById(R.id.txUsuario);
        datos = this.getIntent().getExtras();
        String este = datos.getString("idUsuario");
        usuario.setText(este);
    }

    public void actualizaCorreo(View v){ // Se actualiza el correo de la cuenta con el correo ingresado, si no se puede se marca error
        Conexion con = new Conexion(this, "ProyectoDiabetes", null, 1);
        SQLiteDatabase db = con.getWritableDatabase();
        ContentValues registro = new ContentValues();
        registro.put("email",correo.getText().toString());

        int res=db.update("usuario",registro,"idUsuario='"+usuario.getText().toString()+"'",null);
        db.close();

        if(res==1){
            Toast.makeText(this, "Se modificaron los datos del paciente", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(this, "NO se actualizaron los datos", Toast.LENGTH_LONG).show();
        }
    }


    public void irMenu(View v){ // Regresa al menú
        Intent i = new Intent(this, Menu.class);
        i.putExtras(datos);
        startActivity(i);
    }
}
