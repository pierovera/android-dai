package com.example.android.adroids;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Menu extends AppCompatActivity {

    TextView usuario;
    Bundle datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) { //Se recuperan los valores de los campos de texto
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        usuario=findViewById(R.id.txUsuario);
        datos = this.getIntent().getExtras();
        String este = datos.getString("idUsuario");
        usuario.setText(este);
    }

    public void ingresaNiveles(View v){ // Ir a la pantalla de ingresar niveles
        Intent i = new Intent(this, Niveles.class);
        i.putExtras(datos);
        startActivity(i);
    }

    public void ingresaRecetas(View v){ // Ir a la pantalla de ingresar recetas
        Intent i = new Intent(this, NuevasRecetas.class);
        i.putExtras(datos);
        startActivity(i);
    }

    public void verRecetas(View v){ // Ir a la pantalla de ver recetas
        Intent i = new Intent(this, VerRecetas.class);
        i.putExtras(datos);
        startActivity(i);
    }

    public void consultaNiveles(View v){ // Ir a la pantalla de consultar niveles
        Intent i = new Intent(this, VerNiveles.class);
        i.putExtras(datos);
        startActivity(i);
    }

    public void modificaSesion(View v){ // Ir a la pantalla de modificar cuenta
        Intent i = new Intent(this, Modifica.class);
        i.putExtras(datos);
        startActivity(i);
    }

    public void cerrarSesion(View v){ // Cerrar la sesión e ir a la pantalla de inicio
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    public void aprendeWeb(View v){ // Ir a la pantalla de web
        Intent i = new Intent(this, Web.class);
        startActivity(i);
    }
}
