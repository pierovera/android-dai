package com.example.android.adroids;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class Crea extends AppCompatActivity {

    EditText usuario,contraseña,nombre,email,sexo;

    @Override
    protected void onCreate(Bundle savedInstanceState) { //Se recuperan los valores de los campos de texto
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crea);
        usuario=findViewById(R.id.etUsuario);
        contraseña=findViewById(R.id.etContraseña);
        nombre=findViewById(R.id.etNombre);
        email=findViewById(R.id.etEmail);
        sexo=findViewById(R.id.etSexo);
    }

    public void crearCuenta(View v) { // Intenta crear la cuenta y marca error si no se puede
        Conexion con = new Conexion(this, "ProyectoDiabetes", null, 1);
        SQLiteDatabase db = con.getWritableDatabase();
        ContentValues registro = new ContentValues();
        registro.put("idUsuario", usuario.getText().toString());
        registro.put("contraseña", contraseña.getText().toString());
        registro.put("nombre", nombre.getText().toString());
        registro.put("email", email.getText().toString());
        registro.put("sexo", sexo.getText().toString());
        if(db.insert("usuario", null, registro)>0)
            Toast.makeText(this, "Se creó la cuenta", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, "NO se pudo crear la cuenta", Toast.LENGTH_LONG).show();
    }

    public void irInicio(View v){ // Regresa a la pantalla de inicio
        Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}
