package com.example.android.adroids;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Conexion extends SQLiteOpenHelper {
    public Conexion(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context,name,factory,version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) { // Creamos las tablas en la base de datos
        sqLiteDatabase.execSQL("create table usuario(idUsuario text primary key, contraseña text, nombre text, email text, sexo text)");
        sqLiteDatabase.execSQL("create table glucosa(idGlucosa integer primary key, nivelG text, recAzucar text, recCarb text, recGrasas text, fecha text, idUsuario text)");
        sqLiteDatabase.execSQL("create table receta(idReceta integer primary key,nombreR text, ingredientes text, instrucciones text, carbohidratos double, idUsuario text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) { //eliminamos las tablas existentes en la base de datos
        sqLiteDatabase.execSQL("drop table if exists usuario");
        sqLiteDatabase.execSQL("drop table if exists glucosa");
        sqLiteDatabase.execSQL("drop table if exists recetas");
    }
}
