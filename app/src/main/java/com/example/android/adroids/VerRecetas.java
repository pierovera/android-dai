package com.example.android.adroids;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class VerRecetas extends AppCompatActivity {

    EditText idR;
    TextView instrucciones, ingredientes, usuario, carbs;
    Spinner nombres;
    Bundle datos;
    String[] recetas;

    @Override
    protected void onCreate(Bundle savedInstanceState) { //Se recuperan los valores de los campos de texto
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_recetas);
        instrucciones=findViewById(R.id.tvIngredientes);
        ingredientes=findViewById(R.id.tvInstrucciones);
        carbs=findViewById(R.id.tvCarbs);
        usuario=findViewById(R.id.txUsuarioB);
        datos = this.getIntent().getExtras();
        String este = datos.getString("idUsuario");
        usuario.setText(este);

        llenaDrop();
    }

    public void verRecetas(View v){ // Busca todas las recetas disponibles al usuario
        Conexion con = new Conexion(this, "ProyectoDiabetes", null, 1);
        SQLiteDatabase db = con.getWritableDatabase();
        Cursor cur = db.rawQuery("select nombreR from receta where idUsuario='"+usuario.getText().toString()+"'",null);


    }

    public void llenaDrop(){ // Llena el drop down list con los nombres de las recetas en la base de datos
        nombres = (Spinner) findViewById(R.id.ddRecetas);
        List<String> list = new ArrayList<String>();

        Conexion con = new Conexion(this, "ProyectoDiabetes", null, 1);
        SQLiteDatabase db = con.getWritableDatabase();
        Cursor cur = db.rawQuery("select nombreR from receta where idUsuario='"+usuario.getText().toString()+"'",null);

        while(cur.moveToNext()){
            list.add(cur.getString(0));
        }
        cur.close();

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nombres.setAdapter(dataAdapter);

        nombres.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String datoSelec = (String) parent.getItemAtPosition(position);
                String[] resp=obtenInfoR(datoSelec);


                carbs.setText(resp[0]);
                ingredientes.setText(resp[1]);
                instrucciones.setText(resp[2]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    public String[] obtenInfoR(String nombreBusca){ // Se busca la receta por el nombre en la base de datos y regresa sus datos
        String[] resp=new String[3];

        Conexion con = new Conexion(this, "ProyectoDiabetes", null, 1);
        SQLiteDatabase db = con.getWritableDatabase();
        Cursor cur = db.rawQuery("select carbohidratos,ingredientes,instrucciones from receta where idUsuario like '" + usuario.getText().toString() + "' and nombreR like '" + nombreBusca + "'", null);

        if (cur.moveToNext()) {
            Double carbb=cur.getDouble(0);
            resp[0]=carbb.toString();
            resp[1]=cur.getString(1);
            resp[2]=cur.getString(2);
        } else {
            Toast.makeText(this, "No existe tal registro. Intentar de nuevo", Toast.LENGTH_LONG).show();
        }
        cur.close();
        return resp;
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    public void irMenu(View v){ // Regresa al menú
        Intent i = new Intent(this, Menu.class);
        i.putExtras(datos);
        startActivity(i);
    }
}
